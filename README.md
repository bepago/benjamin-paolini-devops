Commande pour créer le réseau : 
gcloud compute networks create taw-custom-network --subnet-mode custom

On créer subnet us east 1 :
gcloud compute networks subnets create subnet-us-east1 \
   --network taw-custom-network \
   --region us-east1 \
   --range 10.0.0.0/16

On créer subnet europe west 1 :
gcloud compute networks subnets create subnet-europe-west1 \
   --network taw-custom-network \
   --region europe-west1 \
   --range 10.1.0.0/16

On créer subnet europe west 4 :
gcloud compute networks subnets create subnet-europe-west4 \
   --network taw-custom-network \
   --region europe-west4 \
   --range 10.2.0.0/16

Pour afficher la liste des réseau :
gcloud compute networks subnets list \
   --network taw-custom-network

Créer des règles sur Parefeu : 
gcloud compute firewall-rules create nw101-allow-http \
--allow tcp:80 --network taw-custom-network --source-ranges 0.0.0.0/0 \
--target-tags http

Règles suplémentaire pour ICMP :
gcloud compute firewall-rules create "nw101-allow-icmp" --allow icmp --network "taw-custom-network" --target-tags rules

Règles suplémentaire pour communication interne :
gcloud compute firewall-rules create "nw101-allow-internal" --allow tcp:0-65535,udp:0-65535,icmp --network "taw-custom-network" --source-ranges "10.0.0.0/16","10.2.0.0/16","10.1.0.0/16"

Règles suplémentaire pour SSH :
gcloud compute firewall-rules create "nw101-allow-ssh" --allow tcp:22 --network "taw-custom-network" --target-tags "ssh"

Règles suplémentaire pour RDP :
gcloud compute firewall-rules create "nw101-allow-rdp" --allow tcp:3389 --network "taw-custom-network"

Créer une instance dans un sous réseau :
gcloud compute instances create us-test-01 \
--subnet subnet-us-east1 \
--zone us-east1-c \
--machine-type e2-standard-2 \
--tags ssh,http,rules

gcloud compute instances create us-test-02 \
--subnet subnet-europe-west1 \
--zone europe-west1-b \
--machine-type e2-standard-2 \
--tags ssh,http,rules

gcloud compute instances create us-test-03 \
--subnet subnet-europe-west4 \
--zone europe-west4-c \
--machine-type e2-standard-2 \
--tags ssh,http,rules

